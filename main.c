///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Midterm - Loops
///
/// @file    main.c
/// @version 1.0
///
/// Sums up values in arrays
///
/// Your task is to print out the sums of the 3 arrays...
///
/// The three arrays are held in a structure.  Consult numbers.h for the details.
///
/// For array1[], you'll iterate over using a for() loop.  The correct sum for 
/// array1[] is:  48723737032
/// 
/// For array2[], you'll iterate over it using a while()... loop.
///
/// For array3[], you'll iterate over it using a do ... while() loop.
///
/// Sample output:  
/// $ ./main
/// 11111111111
/// 22222222222
/// 33333333333
/// $
///
/// @brief Midterm - Loops - EE 205 - Spr 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "numbers.h"

int main() {
int sum, sum2, sum3;
int p;
int i;
sum = 0;
sum w= 0;
sum3 =0;
struct ThreeArraysStruct s;


   for(i=0; i<11; i =i +1)
      {
sum = sum + s.array1[i];


      }
printf("%d", sum);

   // Sum array2[] with a while() { } loop and print the result

while(p < 11)
{


sum2 = sum2 +s.array2[p];



p = p + 1;
}
printf("%d", sum2);
   // Sum array3[] with a do { } while() loop and print the result


	return 0;
}

